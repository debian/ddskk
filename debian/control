Source: ddskk
Section: lisp
Priority: optional
Maintainer: Tatsuya Kinoshita <tats@debian.org>
Build-Depends: debhelper-compat (= 13)
Build-Depends-Indep: texinfo
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/debian/ddskk
Vcs-Git: https://salsa.debian.org/debian/ddskk.git
Homepage: https://github.com/skk-dev/ddskk
Rules-Requires-Root: no

Package: ddskk
Architecture: all
Depends: emacsen-common, ${misc:Depends}
Recommends: emacs-nox | emacs, skkdic-cdb | dbskkd-cdb | skkserv
Suggests: skkdic, skkdic-extra, skktools, lookup-el, kakasi, w3m-el, eieio, ruby, dictionaries-common
Provides: skk
Conflicts: skk
Replaces: skk
Description: efficient and characteristic Japanese input system for Emacs
 Daredevil SKK (DDSKK) is a fast and efficient Japanese input system
 written in Emacs Lisp.
 .
 DDSKK is an expand version of SKK (Simple Kana to Kanji conversion
 program, originated by Masahiko Sato).  The way of Kana to Kanji
 conversion is characteristic of SKK.  To learn the usage of DDSKK,
 the tutorial program `skk-tutorial' is available.
 .
 By default, DDSKK tries to connect an skkserv compatible dictionary
 server such as dbskkd-cdb, skksearch or yaskkserv on localhost.
 Directly use of a dictionary file without skkserv is also supported.
 .
 To make a local dictionary from the skkdic package and the skkdic-extra
 package, use the update-skkdic command of the skktools package.  To use
 optional features, install required packages such as lookup-el, kakasi,
 and so on.
